﻿using GooglePlayGames;
using UnityEngine;

public class PlayServicesController : MonoBehaviour
{
    private const string leaderBoard = "CgkI8ua34PQCEAIQAQ";
    void Start()
    {
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
        Social.localUser.Authenticate(success =>
        {
            if (success) { }
            else { }
        });
    }
    
    public void SendScore (int record)
    {
        Social.ReportScore(record, leaderBoard, (bool success) => { });
    }
}
